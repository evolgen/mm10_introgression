#example for the triplet [X]: FRA1; [Y]: KAZ; [I]: IRA

#change the parameter part of the script 'get_dK80.r' for each chromosome and quartet

#here you can find the example for chromosome 19 popX: FRA1 popY: KAY popI: IRA

#see https://github.com/kullrich/distIUPAC how to install the development R package 'distIUPAC'
#see 'help(xyiStats)' to get a full description of the xyiStats function

library(distIUPAC)
library(Biostrings)

### ALTER PARAMETERS START

popX <- "FRA1"
popY <- "KAZ"
popI <- "IRA"

TMP_DIR <- "/tmp"

popX.pos <- 8
popY.pos <- 13
popI.pos <- 12

SEQ_FILE <- "http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/mm10/fasta/pop/chr19.fa.gz"
chr <- "chr19"
OUT_FILE <- paste0(TMP_DIR,"/",popX,".",popY,".",popI,".",chr,".tsv")

WSIZE <- 25000
WJUMP <- 25000

DISTMODEL <- "K80"

### ALTER PARAMETERS END

### MAIN START

dna<-readDNAStringSet(SEQ_FILE)
dna.xyiStats<-xyiStats(dna, x.pos = popX.pos, y.pos = popY.pos, i.pos = popI.pos, wlen = WSIZE,
 wjump = WJUMP, dist = DISTMODEL, threads = 12, x.name = popX, y.name = popY, i.name = popI, chr.name = chr)
write.table(dna.xyiStats, sep="\t", col.names=TRUE, row.names=TRUE, file=OUT_FILE, quote=FALSE)

### END MAIN

quit()
