#example for the triplet [X]: FRA1; [Y]: KAZ; [O]: IRA

#change the parameter part of the script 'get_deltaMean.r' for each chromosome and quartet

#here you can find the example for chromosome 19 popX: FRA1 popY: KAY popI: IRA

#see https://github.com/kullrich/distIUPAC how to install the development R package 'distIUPAC'
#see 'help(xyiStats)' to get a full description of the xyiStats function

library(distIUPAC)
library(Biostrings)

### ALTER PARAMETERS START

popX <- "FRA1"
popY <- "KAZ"
popI <- "IRA"

TMP_DIR <- "/tmp"

popX.pos <- 48:55
popY.pos <- 95:102
popI.pos <- 87:94

SEQ_FILE <- "http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/mm10/fasta/ind/chr19.fa.gz"
chr <- "chr19"
OUT_FILE <- paste0(TMP_DIR,"/",popX,".",popY,".",popI,".",chr,".tsv")

WSIZE <- 25000
WJUMP <- 25000

DISTMODEL <- "IUPAC"

### ALTER PARAMETERS END

### MAIN START

dna<-readDNAStringSet(SEQ_FILE)
dna.xyiStats<-xyiStats(dna, x.pos = popX.pos, y.pos = popY.pos, i.pos = popO.pos, wlen = WSIZE,
 wjump = WJUMP, dist = DISTMODEL, threads = 12, x.name = popX, y.name = popY, i.name = popI, chr.name = chr)
write.table(dna.xyoStats, sep="\t", col.names=TRUE, row.names=TRUE, file=OUT_FILE, quote=FALSE)

### END MAIN

quit()