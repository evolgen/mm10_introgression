# Scripts for the Publication:

Phylogenetic discordance patterns in human and mouse populations suggest frequent introgression from distant non-sampled taxa

Ullrich KK and Tautz D

# Table of contents
1. [Data sources](#sources)
    1. [Data sources - house mouse (mm10)](#mm10_sources)
    2. [Data sources - human (hg19)](#hg19_sources)
2. [Mapping FASTQ](#mapping_fastq)
3. [Mapping FASTA](#mapping_fasta)
4. [Construction of consensus sequences for natural populations - CONSENSUS FASTA](#consensus_fasta)
5. [Construction of individual sequences for natural populations - INDIVIDUAL FASTA](#individual_fasta)
6. [dK80 calculation](#dK80)
7. [Literal distance calculation](#deltaMean)
8. [Simulations](#simulations)
    1. [Neutral simulations](#neutral_simulations)
    2. [Extended neutral simulations](#extended_simulations)
    3. [Ghostflow simulations](#ghostflow_simulations)
    4. [Species specific simulations](#species_simulations)

# Table of example code
1. [EXAMPLE CODE001 - Mapping house mouse individual FASTQ to mm10 reference with ngm](#exp001)
2. [EXAMPLE CODE002 - Artificial illumina reads for house mouse](#exp002)
3. [EXAMPLE CODE003 - Artificial illumina reads for chimp](#exp003)
4. [EXAMPLE CODE004 - Mapping artificial chimp FASTQ to hg19 reference with bwa mem](#exp004)
5. [EXAMPLE CODE005 - Obtain coverage for one individual from the _Mus musculus musculus AFG_ population with ANGSD](#exp005)
6. [EXAMPLE CODE006 - Create CONSENSUS FASTA file for the population _Mus musculus musculus AFG_ with ANGSD](#exp006)
7. [EXAMPLE CODE007 - ANGSD command to create CONSENSUS FASTA file for each population](#exp007)
8. [EXAMPLE CODE008 - ANGSD command to create IUPAC FASTA file for one individual](#exp008)
9. [EXAMPLE CODE009 - dK80 calculation with the R script get_dK80.r](#exp009)
10. [EXAMPLE CODE010 - Running and evaluating results of get_dK80.r script](#exp010)
11. [EXAMPLE CODE011 - deltaMean calculation with the R script get_deltaMean.r](#exp011)
12. [EXAMPLE CODE012 - SCRM commands tD12: 0.1 tD13: 0.4 tD14: 3 tGF: 0.05](#exp012)
13. [EXAMPLE CODE013 - SCRM commands tD12: 0.1 tD13: 0.8 tD14: 3 tGF: 0.05](#exp013)
14. [EXAMPLE CODE014 - SCRM commands tD12: 0.1 tD13: 1.2 tD14: 3 tGF: 0.05](#exp014)
15. [EXAMPLE CODE015 - SCRM commands tD12: 0.025 tD13: 0.5 tD14: 0.6 tGF: 0.0125](#exp015)
16. [EXAMPLE CODE016 - SCRM ghostflow command example](#exp016)
17. [EXAMPLE CODE017 - SCRM commands Fu et al. 2014 - SI 18. Pages 106-107](#exp017)

## Data sources <a name="sources"></a>

Two model species, house mouse and human, were used to infer phylogenetic discordance patterns. For each species a sub-section indicates the data sources (see below).

Within parenthesis you will find the global steps applied to obtain genome mapping files.

BAM : public available BAM

FASTQ -> BAM : see section Mapping FASTQ

FASTA -> FASTQ -> BAM : see section Mapping FASTA

### house mouse (mm10) - sources: <a name="mm10_sources"></a>

For each species the number of individuals per sub-population are indicated and how the BAM files have been obtained (see section Mapping FASTQ or section Mapping FASTA).

#### used species - overview:

- _Mus caroli_ - 1xCAROLI (FASTA -> FASTQ -> BAM)
- _Mus musculus domesticus_ - 8xGER, 8xFRA1, 20xFRA2, 8xIRA (FASTQ -> BAM)
- _Mus musculus musculus_ - 6xAFG, 8xCZE, 8xKAZ (FASTQ -> BAM)
- _Mus musculus castaneus_ - 10xCAS1, 20xCAS2 (FASTQ -> BAM)
- _Mus spretus_ - 8xSPRE (FASTQ -> BAM)
- _Mus spicilegus_ - 1xSPI (FASTQ -> BAM)

- in total 106 individuals

#### population assignment with individual IDs:

- AFG: 396, 413, 416, 424, 435, 444
- CAROLI: mm10.CAROLI_EiJ_v1
- CAS1: H12, H14, H15, H24, H26, H27, H28, H30, H34, H36
- CAS2: tw1000, tw1011, tw1057, tw1143, tw1145, tw1167, tw1172, tw1200, tw1231, tw1237, tw1291, tw1294, tw667, tw746, tw747, tw807, tw813, tw837, tw8514, tw873
- CZE: CR12, CR13, CR16, CR23, CR25, CR29, CR46, CR59
- FRA1: 14, 15B, 16B, 18B, B2C, C1, E1, F1B
- FRA2: wildmus101, wildmus211, wildmus218, wildmus227, wildmus231, wildmus234, wildmus236, wildmus246, wildmus303, wildmus310, wildmus311, wildmus313, wildmus314, wildmus327, wildmus329, wildmus330, wildmus334, wildmus34, wildmus44, wildmus54
- GER: TP1, TP121B, TP17-2, TP3-02, TP4a, TP51D, TP7-10F1A2, TP81B
- IRA: AH15, AH23, JR11, JR15, JR2-F1C, JR5-F1C, JR7-F1C, JR8-F1A
- KAZ: AL1, AL16, AL19, AL33, AL38, AL40, AL42, AL42
- SPI: SPI
- SPRE: SP36, SP39, SP41, SP51, SP62, SP68, SP69, SP70

#### detailed description of the raw-data sources:

Genomic sequence data files (FASTQ) for _Mus musculus domesticus GER_, _Mus musculus domesticus FRA1_, _Mus musculus domesticus IRA_, _Mus musculus musculus AFG_, _Mus musculus castaneus CAS1_ and _Mus spretus SPRE_ were obtained from the European Nucleotide Archive <https://www.ebi.ac.uk/ena> according to table 1 from the original publication ([Harr et al. 2016](http://www.nature.com/articles/sdata201675)) <https://www.nature.com/articles/sdata201675/tables/1> (see also <https://www.ebi.ac.uk/ena/data/view/PRJEB9450>, <https://www.ebi.ac.uk/ena/data/view/PRJEB11742>, <https://www.ebi.ac.uk/ena/data/view/PRJEB14167> and <https://www.ebi.ac.uk/ena/data/view/PRJEB2176>).

Genomic sequence data files (FASTQ) for _Mus musculus domesticus FRA2_ and _Mus musculus castaneus CAS2_ were obtained from the European Nucleotide Archive from the study [PRJEB15873](https://www.ebi.ac.uk/ena/data/view/PRJEB15873).

Genomic sequence data files (FASTQ) for _Mus spicilegus SPI_ were obtained from the European Nucleotide Archive from the study [PRJEB11533](https://www.ebi.ac.uk/ena/data/view/PRJEB11533) (see also <https://www.ebi.ac.uk/ena/data/view/ERX1181178>, <https://www.ebi.ac.uk/ena/data/view/ERX1181179> and <https://www.ebi.ac.uk/ena/data/view/ERX1181177>).

Genome assembly (FASTA) for _Mus caroli CAROLI_ was obtained from [Ensembl](https://www.ensembl.org/index.html) release-90 (see also <ftp://ftp.ensembl.org/pub/release-90/fasta/mus_caroli/dna/Mus_caroli.CAROLI_EIJ_v1.1.dna.toplevel.fa.gz>.

### human (hg19) - sources: <a name="hg19_sources"></a>

For each species the number of individuals per sub-population are indicated and how the BAM files have been obtained (see section Mapping FASTQ or section Mapping FASTA).

#### used species - overview:

- _Homo sapiens_ - 2xAUSTRALIAN, 2xDAI, 2xDINKA, 2xFRENCH, 2xHAN, 2xKARITIANA, 2xMANDENKA, 2xMBUTI, 1xMIXE, 2xPAPUAN, 2xSAN, 2xSARDINIAN, 2xYORUBA, 1xUST
- _Denisovans_ - 1xDENISOVA (BAM)
- _Neandertals_ - 2xALTAI (BAM)
- _Pan paniscus_ - 1xBONOBO (FASTA -> FASTQ -> BAM)
- _Nomascus leucogynes_ - 1xGIBBON (FASTA -> FASTQ -> BAM)
- _Pongo abelii_ - 1xORANG (FASTA -> FASTQ -> BAM)
- _Pan troglodytes_ - 1xCHIMP (FASTA -> FASTQ -> BAM)

- in total 33 individuals

#### population assignment with individual IDs:

- AFRICA: SAN_HGDP01029, SAN_HGDP01036, MBUTI_HGDP00456, MBUTI_HGDP00982, YORUBA_HGDP00927, YORUBA_HGDP00936, MANDENKA_HGDP01284, MANDENKA_HGDP01286, DINKA_DNK02, DINKA_DNK07
- ASIA: DAI_HGDP01307, DAI_HGDP01308, HAN_HGDP00775, HAN_HGDP00778
- AUSTRALIA: BUR_E, WON_M
- BONOBO: hg19.bonobo
- CHIMP: hg19.chimp
- DENISOVA: Denisova.T_hg19_1000g
- EUROPE: FRENCH_HGDP00521, FRENCH_HGDP00533, SARDINIAN_HGDP00665, SARDINIAN_HGDP01076
- GIBBON: hg19.gibbon
- KARITIANA: KAR_HGDP00998, KAR_HGDP01015
- MIXE: MIXE0007
- NEA: AltaiNea.hg19_1000g, Vi33.19
- ORANG: hg19.orang
- PAPUAN: PAP_HGDP00542, PAP_HGDP00546
- UST: Ust_Ishim

#### detailed description of the raw-data sources:

Mapped genomic sequence data files (BAM) for _Homo sapiens_ were obtained from the [Max Planck Institue for Evolutionary Anthropology - Department of Evolutionary Genetics](https://www.eva.mpg.de/genetics/genome-projects.html) (see also <http://cdna.eva.mpg.de/neandertal/altai/AltaiNeandertal/bam/>, <http://cdna.eva.mpg.de/neandertal/altai/ModernHumans/bam/>, <http://cdna.eva.mpg.de/denisova/BAM/human/> and <http://cdna.eva.mpg.de/denisova/alignments/>).

Genome assemblies (FASTA) for _Pan paniscus BONOBO_, _Nomascus leucogenys GIBBON_, _Pongo abelii ORANG_ and _Pan troglodytes CHIMP_ were obtained from the [Max Planck Institue for Evolutionary Anthropology - Department of Evolutionary Genetics](https://www.eva.mpg.de/genetics/genome-projects.html) or from [Ensembl](https://www.ensembl.org/index.html) release-90 (see also <https://bioinf.eva.mpg.de/bonobo/chr.fa.gz>, <ftp://ftp.ensembl.org/pub/release-90/fasta/nomascus_leucogenys/dna/Nomascus_leucogenys.Nleu1.0.dna.toplevel.fa.gz>, <ftp://ftp.ensembl.org/pub/release-90/fasta/pongo_abelii/dna/Pongo_abelii.PPYG2.dna.toplevel.fa.gz> and <ftp://ftp.ensembl.org/pub/release-90/fasta/pan_troglodytes/dna/Pan_troglodytes.CHIMP2.1.4.dna.toplevel.fa.gz>).

## Mapping (FASTQ) <a name="mapping_fastq"></a>

Genome sequencing reads (FASTQ) were mapped against the house mouse reference genome [mm10](ftp://ftp.ensembl.org/pub/release-90/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.primary_assembly.fa.gz) with 'ngm' ([Sedlazeck et al. 2013](https://www.ncbi.nlm.nih.gov/pubmed/23975764)), followed by sorting, marking and removing duplicates with the picard software suite (<https://broadinstitute.github.io/picard/>).

#### EXAMPLE CODE001 - Mapping house mouse individual FASTQ to mm10 reference with ngm <a name="exp001"></a>

```
#example for one Mus musculus musculus individual - AFG1 - 396
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR142/005/ERR1425295/ERR1425295_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR142/006/ERR1425296/ERR1425296_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR142/007/ERR1425297/ERR1425297_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR142/008/ERR1425298/ERR1425298_1.fastq.gz

zcat ERR1425295_1.fastq.gz >> Mmm_AFG.396.1.fq
zcat ERR1425296_1.fastq.gz >> Mmm_AFG.396.1.fq
zcat ERR1425297_1.fastq.gz >> Mmm_AFG.396.1.fq
zcat ERR1425298_1.fastq.gz >> Mmm_AFG.396.1.fq

wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR142/005/ERR1425295/ERR1425295_2.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR142/006/ERR1425296/ERR1425296_2.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR142/007/ERR1425297/ERR1425297_2.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR142/008/ERR1425298/ERR1425298_2.fastq.gz

zcat ERR1425295_2.fastq.gz >> Mmm_AFG.396.2.fq
zcat ERR1425296_2.fastq.gz >> Mmm_AFG.396.2.fq
zcat ERR1425297_2.fastq.gz >> Mmm_AFG.396.2.fq
zcat ERR1425298_2.fastq.gz >> Mmm_AFG.396.2.fq

FWD=Mmm_AFG.396.1.fq
REV=Mmm_AFG.396.2.fq
NGMOUTPUT=Mmm_AFG.396.mm10.sam
RGID=396
RGSM=396

ngm -1 $FWD -2 $REV -r mm10.fasta -o $NGMOUTPUT --no-unal --sensitive -t 24 --no-progress
 --rg-id $RGID --rg-sm $RGSM --rg-lb lib1 --rg-pl illumina --rg-pu unit1 -b

NGMSORTEDOUTPUT=Mmm_AFG.396.mm10.sorted.bam

java -jar picard.jar SortSam I=$NGMOUTPUT O=$NGMSORTEDOUTPUT SO=coordinate

NODUPOUTPUT=Mmm_AFG.396.mm10.sorted.nodup.bam
DUPMETRICS=Mmm_AFG.396.mm10.sorted.duplicate.metrics

java -jar picard.jar MarkDuplicates I=$NGMSORTEDOUTPUT O=$NODUPOUTPUT M=$DUPMETRICS
 REMOVE_DUPLICATES=true
```

+ NextGenMap 0.5.4
+ picard.jar 2.10.7-SNAPSHOT

## Mapping (FASTA) <a name="mapping_fasta"></a>

### house mouse (mm10):

Genome assemblies (FASTA) were used to generate artificial Illumina reads with 'ART' ([Huang et al. 2011](https://academic.oup.com/bioinformatics/article-lookup/doi/10.1093/bioinformatics/btr708)) to mimic the sequence machine conditions for the house mouse data (HS20).

#### EXAMPLE CODE002 - Artificial illumina reads for house mouse <a name="exp002"></a>

```
#example for the sequence machine HS20 with a read length of 100bp, a coverage of 20, a insert size length of 250bp and a standard deviation of insert size of 80bp
art_illumina -sam -na -ss HS20 -i Rattus_norvegicus.Rnor_6.0.dna.toplevel.fa -f 20 -l 100 -m 250 -s 80 -p -o Rnor_6.0.art_illumina.HS20.f20.l100.m250.s80
```

Subsequently, the artificial Illumina reads were mapped against the house mouse reference genome [mm10](ftp://ftp.ensembl.org/pub/release-90/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.primary_assembly.fa.gz) with 'ngm' ([Sedlazeck et al. 2013](https://www.ncbi.nlm.nih.gov/pubmed/23975764)), followed by sorting, marking and removing duplicates with the picard software suite (<https://broadinstitute.github.io/picard/>) as indicated above (see Mapping FASTQ).

_used software:_

+ ART_Illumina Q Version 2.5.8 (June 7, 2016)
+ NextGenMap 0.5.4
+ picard.jar 2.10.7-SNAPSHOT

### human (hg19)

Genome assemblies (FASTA) were used to generate artificial Illumina reads with 'ART' ([Huang et al. 2011](https://academic.oup.com/bioinformatics/article-lookup/doi/10.1093/bioinformatics/btr708)) to mimic the sequence machine conditions for the human data (GA2).

#### EXAMPLE CODE003 - Artificial illumina reads for chimp <a name="exp003"></a>

```
#example for the sequence machine GA2 with a read length of 75bp, a coverage of 25, a insert size length of 270bp and a standard deviation of insert size of 70bp
art_illumina -sam -na -ss HS20 -i Pan_troglodytes.CHIMP2.1.4.dna.toplevel.fa -f 25 -l 75 -m 270 -s 70 -p -o Pan_troglodytes.CHIMP2.1.4.dna.toplevel.GA2.f25.l75.m270.s70
```

Subsequently, the artificial Illumina reads were mapped against the human reference genome [hg19](http://hgdownload.cse.ucsc.edu/goldenPath/hg19/chromosomes/) supplemented with the Human herpesvirus 4 genomic DNA [NC_007605](https://www.ncbi.nlm.nih.gov/nuccore/NC_007605.1?report=fasta) with 'bwa' ([Li and Durbin 2010](https://www.ncbi.nlm.nih.gov/pubmed/20080505)), followed by sorting, marking and removing duplicates with the picard software suite (<https://broadinstitute.github.io/picard/>) and indel realignment with GATK (<https://software.broadinstitute.org/gatk/>).

#### EXAMPLE CODE004 - Mapping artificial chimp FASTQ to hg19 reference with bwa mem <a name="exp004"></a>

```
#example for chimp
FWD=Pan_troglodytes.CHIMP2.1.4.dna.toplevel.GA2.f25.l75.m270.s70.1.fq
REV=Pan_troglodytes.CHIMP2.1.4.dna.toplevel.GA2.f25.l75.m270.s70.2.fq
BWAOUTPUT=hg19.chimp.sam

bwa mem -t 24 -M -R @RG\tID:chimp\tLB:lib1\tPL:ILLUMINA\tPU:unit1\tSM:chimp -o $BWAOUTPUT hg19.fasta
 $FWD $REV

BWASORTEDOUTPUT=hg19.chimp.sorted.bam

java -jar picard.jar SortSam I=$BWAOUTPUT O=$BWASORTEDOUTPUT SO=coordinate

NODUPOUTPUT=hg19.chimp.sorted.nodup.bam
DUPMETRICS=hg19.chimp.sorted.duplicate.metrics

java -jar picard.jar MarkDuplicates I=$BWASORTEDOUTPUT O=$NODUPOUTPUT M=$DUPMETRICS
 REMOVE_DUPLICATES=true

INTERVALLIST=hg19.chimp.sorted.nodup.suspicious.indel.intervals.list

java -jar GenomeAnalysisTK.jar -T RealignerTargetCreator -nt 12 -I $NODUPOUTPUT -R hg19.fasta
 -o $INTERVALLIST

REALIGNOUTPUT=hg19.chimp.sorted.nodup.realigned.bam

java -jar GenomeAnalysisTK.jar -T  IndelRealigner -I $NODUPOUTPUT -R hg19.fasta -targetIntervals
 $INTERVALLIST -o $REALIGNOUTPUT -compress 0 --maxReadsinMemory 100000
```

_used software:_

+ ART_Illumina Q Version 2.5.8 (June 7, 2016)
+ BWA 0.7.16a
+ picard.jar 2.10.7-SNAPSHOT
+ GATK 3.8-0-ge9d806836

## Construction of consensus sequences for natural populations - CONSENSUS FASTA <a name="consensus_fasta"></a>

To construct population specific consensus sequences, we used 'ANGSD' ([Korneliussen et al. 2014](http://www.biomedcentral.com/1471-2105/15/356/abstract)) and the 'doFasta 2' option.

In addition to the 'doFasta 2' option, which extracts the most common nucleotide, other filters like 'minQ', 'minMapQ' and coverage filters like 'setMinDepth' were applied.

To be able to consider low and high coverage regions, for each individual we evaluated the mean and standard deviation coverage with 'ANGSD' and the 'doDepth' option and calculated the average coverage for each population to be used for the FASTA construction.

NOTE: Population consensus sequences were merged for each chromosome.

All CONSENSUS FASTA files can be obtained from:

### house mouse (mm10)
<http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/mm10/fasta/pop/>


### human (hg19)
<http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/hg19/fasta/pop/>

#### EXAMPLE CODE005 - Obtain coverage for one individual from the _Mus musculus musculus AFG_ population with ANGSD <a name="exp005"></a>

```
#example for individual 396 from the population Mmm_AFG

#used BAM files:

#Mmm_AFG.396.mm10.sorted.nodup.bam

angsd -doDepth 1 -doCounts 1 -minQ 20 -minMapQ 30 -maxDepth 99999
 -i Mmm_AFG.396.mm10.sorted.nodup.bam -out Mmm_AFG.396.mm10.sorted.nodup.bam.coverage
```

#### EXAMPLE CODE006 - Create CONSENSUS FASTA file for the population _Mus musculus musculus AFG_ with ANGSD <a name="exp006"></a>

```
#CONSENSUS FASTA file example for populaton Mmm_AFG for chr1

#used BAM files:

#Mmm_AFG.396.mm10.sorted.nodup.bam
#Mmm_AFG.413.mm10.sorted.nodup.bam
#Mmm_AFG.416.mm10.sorted.nodup.bam
#Mmm_AFG.424.mm10.sorted.nodup.bam
#Mmm_AFG.435.mm10.sorted.nodup.bam
#Mmm_AFG.444.mm10.sorted.nodup.bam

echo Mmm_AFG.396.mm10.sorted.nodup.bam >> AFG.list
echo Mmm_AFG.413.mm10.sorted.nodup.bam >> AFG.list
echo Mmm_AFG.416.mm10.sorted.nodup.bam >> AFG.list
echo Mmm_AFG.424.mm10.sorted.nodup.bam >> AFG.list
echo Mmm_AFG.435.mm10.sorted.nodup.bam >> AFG.list
echo Mmm_AFG.444.mm10.sorted.nodup.bam >> AFG.list

angsd -doFasta 2 -doCounts 1 -maxDepth 99999 -minQ 20 -minMapQ 30 -minInd 3 -minIndDepth 5
 -setMinDepthInd 5 -setMinDepth 15 -setMaxDepthInd 59 -setMaxDepth 354 -b AFG.list
 -r chr1 -out AFG.chr1
```

The CONSENSUS FASTA files were constructed for each population following the same 'ANGSD' command as follows, the individual variables used for each population can be found in the table:

#### EXAMPLE CODE007 - ANGSD command to create CONSENSUS FASTA file for each population <a name="exp007"></a>

```
angsd -doFasta 2 -doCounts 1 -maxDepth 99999 -minQ 20 -minMapQ 30 -minInd ${minInd} -minIndDepth 5 -setMinDepthInd 5 -setMinDepth ${minDepth} -setMaxDepthInd ${setMaxDepthInd} -setMaxDepth ${setMaxDepth} -b ${populationList} -r ${chr} -out ${population}.${chr}
```

### Average coverage table for house mouse (mm10) populations
|POPULATION| MEAN | SD  |MEAN+3xSD|numInd|minInd|minDepth|setMaxDepthInd|setMaxDepth|
|:--------:|:----:|:---:|:-------:|:----:|:----:|:------:|:------------:|:---------:|
|AFG       | 17.77|13.59|    58.55|   6|       3|      15|            59|        354|
|CAROLI    | 14.86| 6.23|    33.55|   1|       1|       5|            34|         34|
|CAS1      | 14.01| 7.57|    36.73|  10|       5|      25|            37|        370|
|CAS2      | 10.39| 9.62|    39.24|  20|      10|      50|            40|        800|
|CZE       | 24.34|14.10|    66.65|   8|       4|      20|            67|        536|
|FRA1      | 21.50|10.03|    51.58|   8|       4|      20|            52|        416|
|FRA2      |  9.03| 5.09|    24.30|  20|      10|      50|            25|        500|
|GER       | 21.64|10.52|    53.19|   8|       4|      20|            54|        432|
|IRA       | 20.25| 9.82|    49.71|   8|       4|      20|            50|        400|
|KAZ       | 25.12|15.97|    73.05|   8|       4|      20|            74|        592|
|SPIC      | 25.14|24.63|    99.02|   1|       1|       5|           100|        100|
|SPRE      | 24.88|14.22|    67.53|   8|       4|      20|            68|        544|

### Average coverage table for human (hg19) populations
|POPULATION| MEAN | SD  |MEAN+3xSD|numInd|minInd|minDepth|setMaxDepthInd|setMaxDepth|
|:--------:|:----:|:---:|:-------:|:----:|:----:|:------:|:------------:|:---------:|
|AFRICA    | 29.31|49.72|   178.47|    10|     5|      25|           179|       1790|
|ASIA      | 28.72|50.72|   180.87|     4|     2|      10|           181|        724|
|BONOBO    | 16.22| 4.91|    30.95|     1|     1|       5|            31|         31|
|CHIMP     | 16.53| 6.86|    37.11|     1|     1|       5|            38|         38|
|DENISOVA  | 28.57|39.04|   145.69|     1|     1|       5|           146|        146|
|EUROPE    | 21.26|32.18|   117.80|     4|     2|      10|           188|        752|
|GIBBON    | 14.49| 6.89|    35.17|     1|     1|       5|            36|         36|
|NEA       | 30.18|46.29|   169.04|     2|     1|       5|           146|        292|
|ORANG     | 15.83| 7.67|    38.83|     1|     1|       5|            39|         39|

_used software:_

+ ANGSD 0.921-9-ga30d0b4 (htslib: 1.7-24-gaa1a775) <http://www.popgen.dk/angsd/index.php/ANGSD>

## Construction of individual sequences using IUPAC code - INDIVIDUAL IUPAC FASTA <a name="individual_fasta"></a>

To construct individual sequences, we used 'ANGSD' and the 'doFasta 4' option. For this option, after all filters have been applied, all multi-allelic sites will be encoded as IUPAC code.

In addition to the 'doFasta 4' option, which extracts IUPAC code, other filters like 'minQ', 'minMapQ' and coverage filters like 'setMinDepth' were applied (see also <https://github.com/kullrich/distIUPAC/blob/master/vignettes/bam2IUPAC.Rmd>).

NOTE: Individual sequences were merged for each chromosome.

All INDIVIDUAL IUPAC FASTA files can be obtained from:

### house mouse (mm10)
<http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/mm10/fasta/ind/>

### human (hg19)
<http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/hg19/fasta/ind/>

INDIVIDUAL IUPAC FASTA example for one individual from the population _Mus musculus musculus AFG_

#### EXAMPLE CODE008 - ANGSD command to create IUPAC FASTA file for one individual <a name="exp008"></a>
```
#example for one Mus musculus musculus individual - AFG1 - 396 for chr1

#used BAM file:

#Mmm_AFG.396.mm10.sorted.nodup.bam

angsd -doFasta 4 -doCounts 1 -minQ 20 -minMapQ 30 -setMinDepth 5 -i Mmm_AFG.396.mm10.sorted.nodup.bam
 -r chr1 -out AFG.396.ngm.minQ20.minMapQ30.setMinDepth5.chr1
```

_used software:_

+ ANGSD 0.921-9-ga30d0b4 (htslib: 1.7-24-gaa1a775) <http://www.popgen.dk/angsd/index.php/ANGSD>

## dK80 distance calculation <a name="dK80"></a>

### Calculate dK80 distance between populations using the CONSENSUS FASTA pseudo-genome files

To calculate dK80 distance between populations, triplets (([$`I`$], [$`X`$]), [$`Y`$]) are used. Within the population triplets, dK80 is calculated on non-overlapping sequence windows ($`w`$) throughout the investigated genome between this population triplet on a window ($`w_{i}`$) as:
```math
dK80_{XYI_{w_{i}}} = d_{XY_{w_{i}}} - d_{XI_{w_{i}}}
```
where $`d_{XY_{w_{i}}}`$ and $`d_{XI_{w_{i}}}`$ are defined as the average Kimura's 2-parameter sequence distance ([Kimura 1980](https://www.ncbi.nlm.nih.gov/pubmed/7463489)) between the corresponding two populations calculated with the function 'dist.dna' of the of the R package 'ape' ([Paradis et al. 2004](https://academic.oup.com/bioinformatics/article/20/2/289/204981/APE-Analyses-of-Phylogenetics-and-Evolution-in-R)) using the model 'K80'. Prior the calculation of dK80 all sites with missing data within the specified window ($`w_{i}$) and the specified populations were removed in a pairwise fashion dist.dna(x, model="K80", pairwise.deletion=TRUE). Sequence windows were parsed with the R development package 'distIUPAC' (<https://github.com/kullrich/distIUPAC>) and the 'xyiStats' function.

The 'xyiStats' function calculates distances comparing two populations (x: receiver; y: donor) with an ingroup population (i: ingroup). In a four-taxon scenario ((($`P_1`$,$`P_2`$),$`P_3`$),$`O`$) with geneflow from $`P_3`$>>$`P_2`$, the populations should be defined for deltaMean and deltaMin statistics as follows [x:P2 y:P3 i:P1].
Accordingly in the four-taxon scenario ((($`P_1`$,$`P_2`$),$`P_3`$),$`O`$) with geneflow from $`P_2`$>>$`P_3`$, the populations should be defined for deltaMean and deltaMin statistics as follows [x:P3 y:P2 i:P1].

NOTE: For each triplet comparison all analyzed chromosomes were merged into one file.

All dK80 files can be obtained from:

### house mouse (mm10)

<http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/hg19/dK80/pop/>

The following triplets have been tested:

triplet combinations:

- [X]: FRA1 [Y]: KAZ [I]: IRA
- [X]: FRA1 [Y]: CAS [I]: IRA
- [X]: FRA1  [Y]: SPRE [I]: IRA

dK80 calculation example for chromosome 19 for the triplet [X/$`P_2`$]: _Mus musculus domesticus FRA1; [Y/$`P_3`$]: _Mus musculus musculus KAZ_; [I/$`P_1`$]:_Mus musculus domesticus IRA_

#### EXAMPLE CODE009 - dK80 calculation with the R script get_dK80.r <a name="exp009"></a>
```
#example for the triplet [X]: FRA1; [Y]: KAZ; [I]: IRA

#change the parameter part of the script 'get_dK80.r' for each chromosome and quartet

#here you can find the example for chromosome 19 popX: FRA1 popY: KAY popI: IRA

#see https://github.com/kullrich/distIUPAC how to install the development R package 'distIUPAC'
#see 'help(xyiStats)' to get a full description of the xyiStats function

library(distIUPAC)

### ALTER PARAMETERS START

popX <- "FRA1"
popY <- "KAZ"
popI <- "IRA"

TMP_DIR <- "/tmp"

popX.pos <- 8
popY.pos <- 13
popI.pos <- 12

SEQ_FILE <- "http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/mm10/fasta/pop/chr19.fa.gz"
chr <- "chr19"
OUT_FILE <- paste0(TMP_DIR,"/",popX,".",popY,".",popI,".",chr,".tsv")

WSIZE <- 25000
WJUMP <- 25000

DISTMODEL <- "K80"

### ALTER PARAMETERS END

### MAIN START

dna<-readDNAStringSet(SEQ_FILE)
dna.xyiStats<-xyiStats(dna, x.pos = popX.pos, y.pos = popY.pos, i.pos = popI.pos, wlen = WSIZE,
 wjump = WJUMP, dist = DISTMODEL, threads = 12, x.name = popX, y.name = popY, i.name = popI, chr.name = chr)
write.table(dna.xyiStats, sep="\t", col.names=TRUE, row.names=TRUE, file=OUT_FILE, quote=FALSE)

### END MAIN

quit()
```

#### EXAMPLE CODE010 - Running and evaluating results of get_dK80.r script <a name="exp010"></a>
```
#to run the script

nohup nice R CMD BATCH --vanilla get_dK80.r &

#to check result file

tail -f /tmp/FRA1.AFG.IRA.chr19.tsv
```

### human (hg19)

http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/hg19/dK80/pop/

The following triplets have been tested:

triplet combinations:

- [X]: EUROPE [Y]: NEA [I]: AFRICA
- [X]: ASIA   [Y]: NEA [I]: AFRICA
- [X]: EUROPE [Y]: DENISOVA [I]: AFRICA

_used software:_

+ R version 3.6.1 (2019-07-05)
+ R package ape_5.3
+ R package Biostrings_2.52.0
+ R package S4Vectors_0.22.0
+ R package XVector_0.24.0
+ R package IRanges_2.18.1
+ R package BiocGenerics_0.30.0
+ R package distIUPAC_0.1.0
+ R package Rcpp_1.0.2
+ R package doMC_1.3.6
+ R package foreach_1.4.7
+ R package grid_3.6.1
+ R package RcppThread_0.5.3
+ R package rlist_0.4.6.1
+ R package devtools_2.2.0

+ get_dK80.r <https://gitlab.gwdg.de/evolgen/mm10_introgression/blob/master/scripts/get_dK80.r>


### Calculate literal distance between populations using the IUPAC FASTA pseudo-genome files <a name="deltaMean"></a>

As for the calculation of dK80 values and a single consensus sequences per population, it is possible to calculate mean values per population using all individuals. However, the individual FASTA files are IUPAC encoded. We developed an R package (https://github.com/kullrich/distIUPAC) to calculate pairwise 'literal distances' using IUPAC encoded nucleotide sequences. Briefly, only bi-allelic nucleotide states are considered for the pairwise distance calculation and all tri-nucleotide states, gaps or missing data are discarded. The distance score matrix applied is based on ([Chang et al. 2017](https://www.ncbi.nlm.nih.gov/pubmed/28819774)), whereby two homologous SNPs yield 0 if they were identical or identically heterozygous, 1 if different, and 0.5 if one of them was heterozygous (see 'scoreMatrix' function). 

deltaMean calculation example for chromosome 19 for the triplet [X/$`P_2`$]: _Mus musculus domesticus_ FRA1; [Y/$`P_3`$]: _Mus musculus musculus_ KAZ; [I/$`P_1`$]:_Mus musculus domesticus_ IRA

#### EXAMPLE CODE011 - deltaMean calculation with the R script get_deltaMean.r <a name="exp011"></a>
```
#example for the triplet [X]: FRA1; [Y]: KAZ; [O]: IRA

#change the parameter part of the script 'get_deltaMean.r' for each chromosome and quartet

#here you can find the example for chromosome 19 popX: FRA1 popY: KAY popI: IRA

#see https://github.com/kullrich/distIUPAC how to install the development R package 'distIUPAC'
#see 'help(xyiStats)' to get a full description of the xyiStats function

library(distIUPAC)

### ALTER PARAMETERS START

popX <- "FRA1"
popY <- "KAZ"
popI <- "IRA"

TMP_DIR <- "/tmp"

popX.pos <- 48:55
popY.pos <- 95:102
popI.pos <- 87:94

SEQ_FILE <- "http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/mm10/fasta/ind/chr19.fa.gz"
chr <- "chr19"
OUT_FILE <- paste0(TMP_DIR,"/",popX,".",popY,".",popI,".",chr,".tsv")

WSIZE <- 25000
WJUMP <- 25000

DISTMODEL <- "IUPAC"

### ALTER PARAMETERS END

### MAIN START

dna<-readDNAStringSet(SEQ_FILE)
dna.xyiStats<-xyiStats(dna, x.pos = popX.pos, y.pos = popY.pos, i.pos = popI.pos, wlen = WSIZE,
 wjump = WJUMP, dist = DISTMODEL, threads = 12, x.name = popX, y.name = popY, i.name = popI, chr.name = chr)
write.table(dna.xyoStats, sep="\t", col.names=TRUE, row.names=TRUE, file=OUT_FILE, quote=FALSE)

### END MAIN

quit()
```

## Simulations <a name="simulations"></a>

Simulations were performed to evaluate dK80 under neutral scenarios and under species-specific published phylogenetic scenario mimicking the real data.

### Neutral simulations <a name="neutral_simulations"></a>

To evaluate the dK80 statistics, simulated data sets were generated mimicking the scenarios of small sequence windows given in [Martin et al. 2015](https://doi.org/10.1093/molbev/msu269).

For each recombination data set ($`4Nr0.1`$, $`4Nr0.01`$ and $`4Nr0.001`$) 100 sequence windows were simulated following the split times, populations relationships ((($`P_1`$,$`P_2`$),$`P_3`$),$`O`$) and admixture scenarios ($`P_2`$ gene flow into $`P_3`$ and $`P_3`$ gene flow int $`P_2`$) as given in [Martin et al. 2015](https://doi.org/10.1093/molbev/msu269).

The entire set of simulations was repeated with four different window sizes: 1, 5, 10 and 25kbp with custom R scripts using the R package scrm [Staab et al. 2015](https://doi.org/10.1093/bioinformatics/btu861) and Seq-Gen [Rambuat and Grass 1997](https://doi.org/10.1093/bioinformatics/13.3.235).

See an overview of simulated data sets here:

<img src="/png/neutral_simulations_according_to_Martin_2015.png"  width="50%" height="50%">

NOTE: For each simulated population the CONSENSUS and INDIVIDUAL IUPAC sequences were calculated and used for further downstream calculation of dK80/deltaMean.

All neutral simulation files can be obtained from:

#### $`P_2`$ gene flow into $`P_3`$

http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/neutral_simulations/Martin2015/P2P3.RData

#### $`P_3`$ gene flow into $`P_2`$

http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/neutral_simulations/Martin2015/P3P2.RData

Scripts to generate the output files can be obtained from:

http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/neutral_simulations/Martin2015/4Nr0.1/scripts/
http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/neutral_simulations/Martin2015/4Nr0.01/scripts/
http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/neutral_simulations/Martin2015/4Nr0.001/scripts/

All neutral simulation plots can be obtained from:

http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/neutral_simulations/Martin2015/plots/

_used software:_

+ R version 3.6.1 (2019-07-05)
+ R package ape_5.3
+ R package Biostrings_2.52.0
+ R package S4Vectors_0.22.0
+ R package XVector_0.24.0
+ R package IRanges_2.18.1
+ R package BiocGenerics_0.30.0
+ R package distIUPAC_0.1.0
+ R package Rcpp_1.0.2
+ R package doMC_1.3.6
+ R package foreach_1.4.7
+ R package grid_3.6.1
+ R package RcppThread_0.5.3
+ R package rlist_0.4.6.1
+ R package devtools_2.2.0
+ R package scrm_1.7.2-1

+ get_deltaMean.r <https://gitlab.gwdg.de/evolgen/mm10_introgression/blob/master/scripts/get_deltaMean.r>

### Additional neutral simulations <a name="extended_simulations"></a>

The neutral simulations were extended by scenarios of varying split times to reflect the population split times of house mouse and humans.

Parameter for SCRM and the following split times:

tD12: 0.1 tD13: 0.4 tD14: 3 tGF: 0.05

```
#PARAMETER FOR SCRM
nsam = 68
nreps = 100
len = 25000
N0 = 1e5
mu = 25*1e-9
rec = 25*1e-9
scrm.t = 4*N0*mu*len = 250
scrm.r = 4*N0*rec*len = 250
#SET SEQGEN SCALE BASED ON PARAMETER
scale = 4*N0*mu = 0.01
```

#### EXAMPLE CODE012 - SCRM commands tD12: 0.1 tD13: 0.4 tD14: 3 tGF: 0.05 <a name="exp012"></a>
```
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 1.0 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.9 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.8 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.7 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.6 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.5 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.4 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.3 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.2 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.1 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.0 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
```

Parameter for SCRM and the following split times:

tD12: 0.1 tD13: 0.8 tD14: 3 tGF: 0.05

```
#PARAMETER FOR SCRM
nsam = 68
nreps = 100
len = 25000
N0 = 1e5
mu = 25*1e-9
rec = 25*1e-9
scrm.t = 4*N0*mu*len = 250
scrm.r = 4*N0*rec*len = 250
#SET SEQGEN SCALE BASED ON PARAMETER
scale = 4*N0*mu = 0.01
```

#### EXAMPLE CODE013 - SCRM commands tD12: 0.1 tD13: 0.8 tD14: 3 tGF: 0.05 <a name="exp013"></a>
```
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 1.0 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.9 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.8 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.7 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.6 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.5 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.4 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.3 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.2 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.1 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.0 -ej 0.05 5 3 -ej 0.1 2 1 -ej 0.8 3 1 -ej 3 4 1
```

Parameter for SCRM and the following split times:

tD12: 0.1 tD13: 1.2 tD14: 3 tGF: 0.05

```
#PARAMETER FOR SCRM
nsam = 68
nreps = 100
len = 25000
N0 = 1e5
mu = 25*1e-9
rec = 25*1e-9
scrm.t = 4*N0*mu*len = 250
scrm.r = 4*N0*rec*len = 250
#SET SEQGEN SCALE BASED ON PARAMETER
scale = 4*N0*mu = 0.01
```

#### EXAMPLE CODE014 - SCRM commands tD12: 0.1 tD13: 1.2 tD14: 3 tGF: 0.05 <a name="exp014"></a>
```
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 1.0 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.9 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.8 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.7 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.6 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.5 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.4 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.3 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.2 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.1 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.05 2 0.0 -ej 0.05 5 3 -ej 0.1 2 1 -ej 1.2 3 1 -ej 3 4 1
```

Parameter for SCRM and the following split times:

tD12: 0.025 tD13: 0.5 tD14: 0.6 tGF: 0.0125

```
#PARAMETER FOR SCRM
nsam = 68
nreps = 100
len = 25000
N0 = 1e5
mu = 25*1e-9
rec = 25*1e-9
scrm.t = 4*N0*mu*len = 250
scrm.r = 4*N0*rec*len = 250
#SET SEQGEN SCALE BASED ON PARAMETER
scale = 4*N0*mu = 0.01
```

#### EXAMPLE CODE015 - SCRM commands tD12: 0.025 tD13: 0.5 tD14: 0.6 tGF: 0.0125 <a name="exp015"></a>
```
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 1.0 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.9 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.8 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.7 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.6 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.5 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.4 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.3 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.2 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.1 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -es 0.0125 2 0.0 -ej 0.0125 5 3 -ej 0.025 2 1 -ej 0.05 3 1 -ej 0.6 4 1
```

All extended neutral simulations can be obtained from:

http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/neutral_simulations/Martin2015appended/

### Ghostflow simulations <a name="ghostflow_simulations"></a>

Ghostflow from unsampled populations were simulated based on an extended neutral model on four populations (((P1,P2),P3,)O) with split times defined as (tD12: 0.1; tD13: 0.4 and tD14: 3.0) and two admixture events at the same timepoint at tGF: 0.05.

One admixture event was set as geneflow from $`P_3`$ into $`P_2`$ at varying degrees of admixture [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0].

A second admixture event was set as geneflow from the outgroup $`O`$ into either $`P_1`$ or $`P_2`$ or $`P3_`$.

All scripts to recalculate the ghostflow simulations can be obtained from:

http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/neutral_simulations/ghostflow/

An example SCRM command with ghostflow from the outgroup population $`O`$ into population $`P_1`$ would look like this:

#### EXAMPLE CODE016 - SCRM ghostflow command example <a name="exp016"></a>
```
#partial admixture from P3>>P2 50%: -eps 0.05 2 3 0.5
#partial admixture from O>>P1 10%: -eps 0.05 1 4 0.9
68 1 -T -t 250 -r 250 25000 -I 4 17 17 17 17 -n 1 1 -n 2 1 -n 3 1 -n 4 1 -eps 0.05 2 3 0.5 -eps 0.05 1 4 0.9 -ej 0.1 2 1 -ej 0.4 3 1 -ej 3 4 1
```

### Species-specific published simulations <a name="species_simulations"></a>

#### human (hg19)

For human (hg19) an inferred joint demographic model ([Fu et al. 2014](https://dx.doi.org/10.1038/nature13810); see SI 18. Pages 106-107) was used to calculate the dK80 statistics on simulated 10Mbp sequences in 25 repetitions and sliding windows of 25kbp. For each population given in the model a CONSENSUS sequence was calculated according to the scrm command below which includes single migration events.

Inferred populations:

- AFRICA (10x)
- EUROPE (10x)
- NEA (2x)

Parameter for SCRM:

```
#PARAMETER FOR SCRM
nsam = 22
nreps = 25
len = 1e7
N0 = 1e4
mu = 20*1e-9
rec = 10*1e-9
scrm.t = 4*N0*mu*len
scrm.r = 4*N0*rec*len
#SET SEQGEN SCALE BASED ON PARAMETER
scale = 4*N0*mu
```

#### EXAMPLE CODE017 - SCRM commands Fu et al. 2014 - SI 18. Pages 106-107 <a name="exp017"></a>
```
#scrm command to simulate a demographic model for human (hg19)
#Fu et al. 2014;  Nature, Volume 514, 23 October 2014, Pages 445, http://dx.doi.org/10.1038/nature13810
#SI 18. - simulation1 - SI Pages 107
#0% mixture
22 1 -T -t 8000 -r 4000 10000000 -I 3 10 10 2 -ej 0.0625 2 1 -en 0.075 3 0.02 -en 0.078 3 1 -ej 0.25 3 1
#3% mixture at 2,500 generations
22 1 -T -t 8000 -r 4000 10000000 -I 3 10 10 2 -em 0.0625 1 3 1200 -ej 0.0625 2 1 -em 0.062525 1 3 0 -en 0.075 3 0.02 -en 0.078 3 1 -ej 0.25 3 1
#25% mixture at 2,500 generations
22 1 -T -t 8000 -r 4000 10000000 -I 3 10 10 2 -em 0.0625 1 3 10000 -ej 0.0625 2 1 -em 0.062525 1 3 0 -en 0.075 3 0.02 -en 0.078 3 1 -ej 0.25 3 1
#50% mixture at 2,500 generations
22 1 -T -t 8000 -r 4000 10000000 -I 3 10 10 2 -em 0.0625 1 3 20000 -ej 0.0625 2 1 -em 0.062525 1 3 0 -en 0.075 3 0.02 -en 0.078 3 1 -ej 0.25 3 1
```

All simulations for human (hg19) can be obtained from:

http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/hg19/simulations/

_used software:_

+ R version 3.6.1 (2019-07-05)
+ R package ape_5.3
+ R package Biostrings_2.52.0
+ R package S4Vectors_0.22.0
+ R package XVector_0.24.0
+ R package IRanges_2.18.1
+ R package BiocGenerics_0.30.0
+ R package distIUPAC_0.1.0
+ R package Rcpp_1.0.2
+ R package doMC_1.3.6
+ R package foreach_1.4.7
+ R package grid_3.6.1
+ R package RcppThread_0.5.3
+ R package rlist_0.4.6.1
+ R package devtools_2.2.0
+ R package scrm_1.7.2-1

+ FuSI18s1.woMigration.mu15e9.rec10e9.N010000.r <http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/hg19/simulations/Fu2014_simulation1/FuSI18s1.woMigration.mu15e9.rec10e9.N010000.r>
+ FuSI18s1.wMigration03.mu15e9.rec10e9.N010000.em2500.r <http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/hg19/simulations/Fu2014_simulation1/FuSI18s1.wMigration03.mu15e9.rec10e9.N010000.em2500.r>
+ FuSI18s1.wMigration25.mu15e9.rec10e9.N010000.em2500.r <http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/hg19/simulations/Fu2014_simulation1/FuSI18s1.wMigration25.mu15e9.rec10e9.N010000.em2500.r>
+ FuSI18s1.wMigration50.mu15e9.rec10e9.N010000.em2500.r <http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_hg19/hg19/simulations/Fu2014_simulation1/FuSI18s1.wMigration50.mu15e9.rec10e9.N010000.em2500.r>

## Mappability

To construct a mappability track for the house mouse genome [mm10](ftp://ftp.ensembl.org/pub/release-90/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.primary_assembly.fa.gz) and human genome [hg19](http://hgdownload.cse.ucsc.edu/goldenPath/hg19/chromosomes/) we used the software GEM ([Derrien et al. 2012](https://www.ncbi.nlm.nih.gov/pubmed/22276185)) with follofing options.

APPROXIMATION THRESHOLD: 7
MAX MISMATCHES: 4
MAX ERRORS: 4
MAX BIG INDEL LENGTH: 15
MIN MATCHED BASES: 80
STRATA AFTER BEST: 1

### house mouse (mm10)
K-MER LENGTH: 100

### human (hg19)
K-MER LENGTH: 75

The bigWig track files represents the mean values for 25kbp windows

- mm10: http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/mm10/gem_mappability/mm10.gem.mappability.25kbp.sorted.bed.bw

- hg19: http://wwwuser.gwdg.de/~evolbio/evolgen/others/UllrichTautz/mm10_rn6_hg19/hg19/gem_mappability/hg19.gem.mappability.25kbp.sorted.bed.bw

_used software:_

+ GEM-mappability build 1.315
